Gitlab -> Rocket.chat webhook server

Supporting hooks are following:
* Push events
* Tag events
* Issue events
* Comment events
* Merge request events

### Setting preffile

1. Generate user id and access token from rocket.chat.
1. Copy pref.toml.example to pref.toml and write pref information.

### Start server

1. pip install flask rocketchat_API tomli
1. flask run

### Gitlab setting

1. Setting -> webhooks -> Add webhooks
1. Set url to your server URL and secret token (if needed).

