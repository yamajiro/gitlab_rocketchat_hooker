from flask import Flask, request, abort
from requests import sessions
from rocketchat_API.rocketchat import RocketChat
import tomli
import re


def escapeformarkdown(string):
    escape_str = re.escape("()_*>#+-=.!~")
    return re.sub(f'([{escape_str}])', "\\\1", string)


def post_rocketchat(channel, message):
    with sessions.Session() as session:
        proxies = None
        if "proxy" in pref:
            proxies = {
                'http': pref["proxy"],
                'https': pref["proxy"]
            }
        rocket = RocketChat(
          user_id=pref["userid"],
          auth_token=pref["token"],
          server_url=pref["url"],
          proxies=proxies,
          session=session
        )
        return rocket.chat_post_message(message, channel=channel).json()


def response_notehook(payload):
    message = [
      f'[{payload["project"]["path_with_namespace"]}]' +
      f'({payload["project"]["http_url"]})でコメントが投稿されました。',
      f'実行者: {payload["user_name"]}',
    ]
    target = ""
    if payload["object_attributes"]["notable_type"] == "Commit":
        target = f'コミット：[{payload["commit"][0:9]}]'
    elif payload["object_attributes"]["notable_type"] == "MergeRequest":
        target = f'Merge Request：[{payload["merge_request"]["title"]}]'
    elif payload["object_attributes"]["notable_type"] == "Issue":
        target = f'Issue：[{payload["issue"]["title"]}]'
    elif payload["object_attributes"]["notable_type"] == "Snippet":
        target = f'スニペット：[{payload["snippet"]["title"]}]'
    else:
        abort(500, "comment type is unknown")
    target = target + f'({payload["object_attributes"]["url"]})'
    message.append(target)
    comment = escapeformarkdown(payload["object_attributes"]["note"])
    message.append(f'コメント：{comment}')
    return post_rocketchat(pref["channel"], '\n'.join(message))


def response_taghook(payload):
    message = [
      f'[{payload["project"]["path_with_namespace"]}]' +
      f'({payload["project"]["http_url"]})でタグが変更されました。',
      f'実行者: {payload["user_name"]}',
    ]
    tagmes = f'タグ名: {payload["ref"]}'
    if "checkout_sha" not in payload:
        tagmes = tagmes + "削除"
    else:
        tagmes = tagmes + "追加"
    message.append(tagmes)
    return post_rocketchat(pref["channel"], '\n'.join(message))


def response_margerequesthook(payload):
    event_mes = {
        "open": "発行されました。",
        "close": "クローズしました。",
        "reopen": "再開されました。",
        "update": "更新されました。",
        "approved": "承認者全員に承認されました。",
        "approval": "ユーザーに承認されました。",
        "unapproved": "却下されました。",
        "unapproval": "ユーザーに却下されました。",
        "merge": "マージされました。"
    }
    title = escapeformarkdown(payload["object_attributes"]["title"])
    description = f'[{payload["project"]["path_with_namespace"]}]' + \
        f'({payload["project"]["http_url"]})でMerge Requestが'
    if "action" in payload["object_attributes"]:
        description = description + \
          event_mes[payload["object_attributes"]["action"]]
    else:
        description = description + event_mes["open"]
    message = [
      description,
      f'実行者: {payload["user"]["username"]}',
      f'Merge Request: [{title}]({payload["object_attributes"]["url"]})',
    ]

    if "assignees" in payload and len(payload["assignees"]) > 0:
        assignees = [x["name"] for x in payload["assignees"]]
        message.append("担当者: "+",".join(assignees))

    if "reviewers" in payload and len(payload["reviewers"]) > 0:
        reviewers = [x["name"] for x in payload["reviewers"]]
        message.append("承認対象者: "+",".join(reviewers))
    return post_rocketchat(pref["channel"], '\n'.join(message))


def response_issuehook(payload):
    event_mes = {
        "open": "発行されました。",
        "close": "クローズしました。",
        "reopen": "再開されました。",
        "update": "更新されました。"
    }
    title = escapeformarkdown(payload["object_attributes"]["title"])

    message = [
      f'[{payload["project"]["path_with_namespace"]}]'
      f'({payload["project"]["http_url"]})でissueが'
      f'{event_mes[payload["object_attributes"]["action"]]}',
      f'実行者: {payload["user_name"]}',
      f'Issue: [{title}]({payload["object_attributes"]["url"]})'
    ]
    if "assignees" in payload and len(payload["assignees"]) > 0:
        assignees = [x["name"] for x in payload["assignees"]]
        message.append("担当者: " + ",".join(assignees))
    return post_rocketchat(pref["channel"], '\n'.join(message))


def response_pushhook(payload):
    message = [
      f'[{payload["project"]["path_with_namespace"]}]'
      f'({payload["project"]["http_url"]})へpushが行われました。',
      f'実行者: {payload["user_name"]}',
    ]
    ref = escapeformarkdown(payload["ref"])
    # delete branch
    if "checkout_sha" not in payload:
        message.append(f'ブランチ削除: {ref}')
    # add branch
    elif payload["before"] == 0:
        message.append(f'ブランチ追加: {ref}')
    # add commits
    # なお、一回のwebhookで詳細情報が記載されるコミットの上限は20。
    # https://docs.gitlab.com/ee/user/project/integrations/webhook_events.html#push-events
    if "commits" in payload:
        message.append("")
        message.append("---")
        message.append("")
        message.append(f'{ref}に追加されたコミット一覧')
        for x in payload["commits"]:
            authorname = escapeformarkdown(x["author"]["name"])
            # []のエスケープが上手く機能しない（バグ？）ので、コード扱いでお茶を濁す
            message.append(f'{authorname}: `{x["title"]}`, ' +
                           f'[{x["id"][0:9]}]({x["url"]})')
    return post_rocketchat(pref["channel"], '\n'.join(message))


hookfunc = {
    "Push Hook": response_pushhook,
    "Tag Push Hook": response_taghook,
    "Issue Hook": response_issuehook,
    "Merge Request Hook": response_margerequesthook,
    "Note Hook": response_notehook
}
app = Flask(__name__)
f = open("pref.toml", 'rb')
pref = tomli.load(f)
f.close()


@app.route("/webhook", methods=['POST'])
def route_post_rocketchat():
    if ("secret" not in pref and "X-gitlab-Token" in request.headers) or \
      ("secret" in pref and
       request.headers["X-gitlab-Token"] != pref["secret"]):
        abort(500, "Webhook Error")
    return hookfunc[request.headers["X-Gitlab-Event"]](request.json)


@app.route("/")
def test():
    return "This is webhook, please access /webhook."
